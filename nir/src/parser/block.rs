use super::{Parse, ParseInput, ParseResult, Symbol};
use crate::ast::Block;
use crate::token;

impl<I> Parse<I> for Block
where
    I: Iterator<Item = Symbol>,
{
    #[inline]
    fn parse(input: &mut ParseInput<I>) -> ParseResult<Self> {
        input.parse::<token::LBrace>()?;
        input.parse::<token::RBrace>()?;

        Ok(Self {})
    }
}
