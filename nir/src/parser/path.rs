use super::{Parse, ParseInput, ParseResult, Separated, Symbol};
use crate::ast::{Ident, Path};
use crate::token::DoubleColon;

impl<I> Parse<I> for Path
where
    I: Iterator<Item = Symbol>,
{
    #[inline]
    fn parse(input: &mut ParseInput<I>) -> ParseResult<Self> {
        let segs: Separated<Ident, DoubleColon> = input.parse()?;
        Ok(Self { segs: segs.items })
    }
}
