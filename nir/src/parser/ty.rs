use super::error::{ExpectedToken, ParseError};
use super::{Parse, ParseInput, ParseResult, Symbol};
use crate::ast::{Ident, Type};
use crate::token::{Reserved, Token};

impl<I> Parse<I> for Type
where
    I: Iterator<Item = Symbol>,
{
    #[inline]
    fn parse(input: &mut ParseInput<I>) -> ParseResult<Self> {
        let next = input.next_unwrap(|| {
            vec![
                ExpectedToken::Type,
                ExpectedToken::Ident,
                ereserved!(LParen),
            ]
        })?;

        let ty = match next.0 {
            Token::Type(ty) => Self::Primitive(ty),
            Token::Ident(name) => Self::Declared(Ident { name }),
            Token::Reserved(Reserved::LParen) => Self::Unit,
            _ => {
                input.error(unexpectedtoken!(
                    next.1,
                    next.0,
                    ExpectedToken::Type,
                    ExpectedToken::Ident,
                    ereserved!(LParen)
                ));
                return Err(());
            }
        };

        Ok(ty)
    }
}
